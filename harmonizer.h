#ifndef HARMONIZER_H
#define HARMONIZER_H

#include <QImage>

// This class is NOT thread safe!!!
class Harmonizer
{
public:
    Harmonizer();
    ~Harmonizer();

    void setImage(const QImage&);

    void setSectorSize(int);
    void setSectorPosition(int);
    long long suggest();
    long long suggest(int); //< suggest, but given sector size.

    unsigned getSectorSize() const;
    unsigned getSectorPosition() const;

    QImage harmonize() const;

private:
    struct Member;
    Member* _;
};

#endif // HARMONIZER_H
