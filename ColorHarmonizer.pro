#-------------------------------------------------
#
# Project created by QtCreator 2013-06-10T00:44:38
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ColorHarmonizer
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    harmonizer.cpp \
    hmath.cpp

HEADERS  += mainwindow.h \
    harmonizer.h \
    hmath.h

FORMS    += mainwindow.ui
