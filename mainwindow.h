#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "harmonizer.h"

#include <QImage>
#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    virtual void keyPressEvent(QKeyEvent*);

    virtual void keyReleaseEvent(QKeyEvent*);

private:
    void harmonizeAndUpdate();

private slots:
    void on_actionOpen_triggered();

    void on_actionSaveAs_triggered();

    void on_pushButtonAdvice_clicked();

    void on_spinBoxSize_valueChanged(int);

    void on_spinBoxPosition_valueChanged(int);

private:
    Ui::MainWindow *ui;
    QImage originImage;
    QImage harmonizedImage;
    Harmonizer harmonizer;
};

#endif // MAINWINDOW_H
