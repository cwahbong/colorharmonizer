#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QEvent>
#include <QFileDialog>
#include <QKeyEvent>
#include <QKeySequence>
#include <QShortcut>
#include <Qt>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QObject::connect(new QShortcut(QKeySequence("Escape"), this), SIGNAL(activated()),
                     this, SLOT(setFocus()));
    QObject::connect(new QShortcut(QKeySequence("A"), this), SIGNAL(activated()),
                     ui->pushButtonAdvice, SLOT(click()));
    QObject::connect(new QShortcut(QKeySequence("L"), this), SIGNAL(activated()),
                     ui->checkBoxLockSize, SLOT(click()));
    QObject::connect(new QShortcut(QKeySequence("P"), this), SIGNAL(activated()),
                     ui->spinBoxPosition, SLOT(setFocus()));
    QObject::connect(new QShortcut(QKeySequence("S"), this), SIGNAL(activated()),
                     ui->spinBoxSize, SLOT(setFocus()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void
MainWindow::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_O &&
        event->modifiers() == Qt::NoModifier) {
        ui->labelImage->setPixmap(QPixmap::fromImage(originImage));
    }
}

void
MainWindow::keyReleaseEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_O &&
        event->modifiers() == Qt::NoModifier) {
        ui->labelImage->setPixmap(QPixmap::fromImage(harmonizedImage));
    }
}

void
MainWindow::on_actionOpen_triggered()
{
    const QString& fileName = QFileDialog::getOpenFileName(this, "Open Image...",
            QString(), tr("Images (*.jpg *.jpeg *.bmp *.png)"));
    if (fileName.isNull()) {
        return;
    }
    originImage.load(fileName);
    harmonizedImage = originImage;
    harmonizer.setImage(originImage);
    ui->labelImage->setPixmap(QPixmap::fromImage(originImage));
    ui->actionSaveAs->setEnabled(true);
    ui->pushButtonAdvice->setEnabled(true);
    ui->spinBoxSize->setEnabled(true);
    ui->spinBoxPosition->setEnabled(true);
    ui->checkBoxLockSize->setEnabled(true);
}

void
MainWindow::on_actionSaveAs_triggered()
{
    const QString& fileName = QFileDialog::getSaveFileName(this, "Save Image As...",
            QString(), tr("JPG Image (*.jpg *.jpeg);;BMP Image(*.bmp);;PNG Image(*.png)"));
    if (fileName.isNull()) {
        return;
    }
    ui->labelImage->pixmap()->save(fileName);
}

void
MainWindow::on_spinBoxSize_valueChanged(int)
{
    if (ui->spinBoxSize->hasFocus()) {
        harmonizeAndUpdate();
    }
}

void
MainWindow::on_spinBoxPosition_valueChanged(int)
{
    if (ui->spinBoxPosition->hasFocus()) {
        harmonizeAndUpdate();
    }
}

void
MainWindow::on_pushButtonAdvice_clicked()
{
    if (ui->checkBoxLockSize->isChecked()) {
        harmonizer.suggest(ui->spinBoxSize->value());
    } else {
        harmonizer.suggest();
    }
    ui->spinBoxSize->setValue(harmonizer.getSectorSize());
    ui->spinBoxPosition->setValue(harmonizer.getSectorPosition());
    harmonizeAndUpdate();
}

void
MainWindow::harmonizeAndUpdate()
{
    harmonizer.setSectorSize(ui->spinBoxSize->value());
    harmonizer.setSectorPosition(ui->spinBoxPosition->value());
    harmonizedImage = harmonizer.harmonize();
    ui->labelImage->setPixmap(QPixmap::fromImage(harmonizedImage));
}
