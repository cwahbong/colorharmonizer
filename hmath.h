#ifndef HMATH_H
#define HMATH_H

double deg2rad(double);

double gauss(double, double, double);

#endif // HMATH_H
