\documentclass[a4paper,12pt]{article}
\usepackage{fancyhdr}
\usepackage[a4paper,margin=2.5cm,top=3.4cm]{geometry}
\usepackage{lastpage}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{url}
\usepackage{graphicx}

\pagestyle{fancy}
\lhead[]{Virtual Reality}
\chead[]{}
\rhead[]{R01922163 Chia-Wei Chang}
\lfoot[]{}
\cfoot[]{Page \thepage{} of \pageref{LastPage}}
\rfoot[]{}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}

\DeclareMathOperator*{\argmin}{arg min}

\begin{document}
\begin{center}
\textbf{\huge{Color Harmonizer}}
\end{center}

\section{Introduction}
This is an application that can harmonize picture in various format.
And we provide some options for users.

Sometimes we want to enhence the harmony among the colors in the
picture.  Color harmonization is such one technique to achieve this.
Cohen-Or et al. proposed a method of color harmonization method that
remains faithful of the image~\cite{Cohen-Or:2006:CH:1141911.1141933}.
We implement color harmonization in one sector and provide fancy
graphical user interface.

Our implementation is efficient enough so that we can harmonize the
image instantly.  That gives the best user experience.

\section{Dependency Informations}

\begin{itemize}
	\item Language: C++.
	\item Library used: Qt.
	\item Build system: qmake, make.
	\item Environment: Windows, Linux(untested), Mac OS(untested).
\end{itemize}


\section{Functionality}
Our application can:

\begin{itemize}
	\item Open the image and save the harmonized image in supported format.
	\item Choose the best central angle of hue sector and orientation.
	\item Choose the best orientation with fixed central angle given by user.
	\item Harmonize the image by given sector size and orientation.
\end{itemize}

\subsection{Feature}
Here we list some fancy features.

\begin{itemize}
	\item Instant harmonization even when you are spinning the value.
	\item Each of the functions has a keyboard short-cut.
\end{itemize}

\subsection{Supported Formats}
Here we list all of the supported image formats.

\begin{itemize}
	\item Jpeg compression (JPG).
	\item Bitmap image (BMP).
	\item Portable Network Graphics (PNG).
\end{itemize}

\section{Usage}
Currently we provide GUI application.  You can find ``Open'' and
``Save'' in the ``File'' menu bar.  The left part of the window is
image area with scroll, and the right part of the window contains
options for harmonization.  You can see the screen shot in
\figurename~\ref{fig:screenshot}.

\begin{figure}[h]
\center
\includegraphics[scale=0.7]{screenshot.jpg}
\caption{Screen shot of the application.}
\label{fig:screenshot}
\end{figure}

With the API we provide you can also create your own harmonizer, or use
this to process your images.

\subsection{Automatic Harmonizing}
Since it is very fast to harmonize, we do not provide button for
starting harmonization.  Instead, we do it automatically.  When you
change any options for harmonization, the application will do the
harmonization automatically, and instantly!  You can always press
keyboard short-cut ``O'' to view the origin image, as we will mention
later.
	
\subsection{Keyboard Short-cuts}
Besides normal open, save, exit, we also provide additional short-cuts
for focusing the spin box or clicking the button directly.  Note that
the short-cuts will not work if you are focusing spin box or other
components. The escape key will help you a lot.  The following
short-cuts are sorted in alphabetical order.

\begin{description}
	\item[$<$A$>$] Click ``Advice'' button.
	\item[$<$L$>$] Check/uncheck the ``Lock size'' check box.
	\item[$<$O$>$] Press to view the origin image, release to view the harmonized image again.
	\item[$<$P$>$] Focus the spin box below ``Size(deg)''.
	\item[$<$S$>$] Focus the spin box below ``Position(deg)''.
	\item[$<$Esc$>$] Defocus the spin box or other components in order to use another short-cuts.
\end{description}

\section{Method and Algorithm}
The algorithm color harmonization can be split into two parts:
template choosing and color shifting.  Both of these two algorithm need
to be fast enough to achieve instant harmonization.  Here we mention
the method of harmonization we used, and some noticeable improvement.  

\subsection{Template Choosing}
By \cite{Cohen-Or:2006:CH:1141911.1141933} we need to find $(m, \alpha)$ to minimize

\[
	F(X, (m, \alpha)) = \sum_{p\in X}\left\|H(p)-E_{T_m(\alpha)}(p)\right\| \cdot S(p),
\]

that is, to find

\[
	\argmin_{(m, \alpha)} F(X, (m, \alpha)).
\]

Where $X$ is the image, $p$ is pixel of image, $m$ is template we
choose, $\alpha$ is orientation of template, $H$ is the hue of pixel,
$E_{T_m(\alpha)}$ is the nearest border of a sector to the hue of pixel.

\subsubsection{Naive method}
One naive method is enumerating all 180 possible size of sector and 360
possible orientations, then calculating its corresponding value of $F$,
finally then take the minimum $F$.  However the time complexity of such
method is related to the number of pixels, a.k.a. size of image,
denoted as $O(|X|)$.

This method takes about some minutes to get the chosen value, it is
obviously not feasible for instant choosing.  We need a faster method
to achieve this.  We propose another method that need pre-process with
also $O|X|$ time, but then constant time to calculate each possible
$F$.  The new method is far more faster then naive method, thus we can
instantly calculate the chosen sector and orientation.

\subsubsection{Improved Method}
Besides defining on a pixel, we also define $E_{T_m(\alpha)}$ and $S$ on
a hue value $h$ here, where $S(h)$ is the sum of saturation of hue-$h$
pixels.  We can rewrite function $F$ into
\[
	F(X, (m, \alpha)) = \sum_{h}\left\|h - E_{T_m(\alpha)}(h)\right\| \cdot S(h).
\]
Since hue is an integer between 0 to 360, we can sum up the total
saturation for each pixel with same hue value while loading the image.
Then use the rewritten equation to calculate $F$ in constant time,
since the number of different hue is constant.

\subsection{Color Shifting}
We only change the hue value of pixels in image.  By
\cite{Cohen-Or:2006:CH:1141911.1141933} we can calculate the new hue
value by
\[
	H'(p) = C + \frac{w}{2}(1-G(\|H(p)-C\|)),
\]
where $C$ is the center orientation of the sector, $w$ is the arc-width
of the sector, and $G_{\sigma}$ is normalized Gaussian function with
$\mu = 0$, $\sigma = \frac{w}{2}$.  Gaussian function will provide
better color balance than linear function
\cite{Cohen-Or:2006:CH:1141911.1141933}.

We can calculate the new hue value in constant time, so the totally
time complexity is $O(|X|)$, where $X$ is the image we want to perform
color shifting.

\bibliographystyle{abbrv}
\bibliography{reference}

\end{document}