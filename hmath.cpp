#include "hmath.h"

#include <cmath>

using std::acos;
using std::exp;
using std::sqrt;

namespace {

const double PI = acos(-1.0);

}

double
deg2rad(double x)
{
    return x / 180.0 * PI;
}

double
gauss(double mean, double stdev, double x)
{
    const double sq = (x - mean) / stdev;
    const double exp_pw = - 0.5 * sq * sq;
    return 1.0 / stdev / sqrt(2.0 * PI) * exp(exp_pw);
}
