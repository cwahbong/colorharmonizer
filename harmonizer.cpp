#include "harmonizer.h"

#include "hmath.h"

#include <QColor>
#include <QtCore/qmath.h>

#include <algorithm>
#include <cstdlib>
#include <limits>

using std::abs;
using std::max;
using std::min;
using std::numeric_limits;

namespace {

long long
fastF(long long satSum[360], int sectorSize, int sectorPosition)
{
    long long result = 0;
    const int side1 = sectorPosition;
    const int side2 = (sectorPosition + sectorSize) % 360;
    if (side2 > side1) {
        for (int h = side2; h < 360; ++h) {
            const int dist = min(abs(h - side1), abs(h - side2));
            result += dist * satSum[h];
        }
        for (int h = 0; h < side1; ++h) {
            const int dist = min(abs(h - side1), abs(h - side2));
            result += dist * satSum[h];
        }
    } else {
        for (int h = side2; h < side1; ++h) {
            const int dist = min(abs(h - side1), abs(h - side2));
            result += dist * satSum[h];
        }
    }
    return result;
}

} // namespace

struct Harmonizer::Member {
    QImage originImage;
    int sectorSize;
    int sectorPosition;
    QColor* imageColors;
    long long satSum[360];
    double gaussTable[360];

    Member() :
        originImage(),
        sectorSize(0),
        sectorPosition(0),
        imageColors(NULL)
    {/* Empty. */}

    ~Member()
    {
        if (imageColors != NULL) {
            delete[] imageColors;
        }
    }

    void
    calcSatSum()
    {
        for (unsigned i = 0; i < 360; ++i) {
            satSum[i] = 0;
        }
        for (int r = 0; r < originImage.height(); ++r) {
            for (int c = 0; c < originImage.width(); ++c) {
                QColor color(originImage.pixel(c, r));
                if (color.hue() >= 0) {
                    satSum[color.hue() % 360] += color.saturation();
                }
            }
        }
    }

    void
    setColors()
    {
        if (imageColors != NULL) {
            delete[] imageColors;
        }
        unsigned imageSize = originImage.width() * originImage.height();
        imageColors = new QColor[imageSize];
        for (int r = 0, i = 0; r < originImage.height(); ++r) {
            for (int c = 0; c < originImage.width(); ++c, ++i) {
                imageColors[i] = QColor(originImage.pixel(c, r)).toHsv();
            }
        }
    }

    void
    calcGauss()
    {
        for (unsigned i = 0; i < 360; ++i) {
            gaussTable[i] = gauss(0, deg2rad(sectorSize/2.0), deg2rad(i));
        }
        for (unsigned i = 1; i < 360; ++i) {
            gaussTable[i] /= gaussTable[0];
        }
        gaussTable[0] = 1.0;
    }

    QColor
    shifted(const QColor& color, int hueCenter, int sectorSize)
    {
        const int hueDiff = color.hue() - hueCenter;
        const int fromCenter = sectorSize * (1.0 - gaussTable[abs(hueDiff)]) / 2.0;
        const int dist = (hueDiff + 360) % 360;
        const int newHue = (hueCenter + (dist <= 180 ? fromCenter : 360 - fromCenter)) % 360;
        return QColor::fromHsv(newHue, color.saturation(), color.value());
    }
};

Harmonizer::Harmonizer() :
    _(new Member())
{/* Empty. */}

Harmonizer::~Harmonizer()
{
    delete _;
}

void
Harmonizer::setImage(const QImage& image)
{
    _->originImage = image;
    _->calcSatSum();
    _->setColors();
}

void
Harmonizer::setSectorSize(int size)
{
    _->sectorSize = size;
}

void
Harmonizer::setSectorPosition(int position)
{
    _->sectorPosition = position;
}

long long
Harmonizer::suggest()
{
    long long result = numeric_limits<long long>::max();
    int bestSize = 0;
    int bestPosition = 0;
    for (int size = 1; size <= 180; ++size) {
        const long long tmp = suggest(size);
        if (tmp < result) {
            bestSize = size;
            bestPosition = _->sectorPosition;
            result = tmp;
        }
    }
    _->sectorSize = bestSize;
    _->sectorPosition = bestPosition;
    return result;
}

long long
Harmonizer::suggest(int size)
{
    _->sectorSize = size;
    long long result = numeric_limits<long long>::max();
    for (int position = 0; position < 360; ++position) {
        const long long tmp = fastF(_->satSum, size, position);
        if (tmp < result) {
            result = tmp;
            _->sectorPosition = position;
        }
    }
    return result;
}

unsigned
Harmonizer::getSectorSize() const
{
    return _->sectorSize;
}

unsigned
Harmonizer::getSectorPosition() const
{
    return _->sectorPosition;
}

QImage
Harmonizer::harmonize() const
{
    _->calcGauss();
    QImage result(_->originImage.width(), _->originImage.height(), QImage::Format_ARGB32);
    const int hueCenter = (_->sectorPosition + _->sectorSize / 2) % 360;
    for (int r = 0, i = 0; r < _->originImage.height(); ++r) {
        QRgb* resultLine = (QRgb*) result.scanLine(r);
        for (int c = 0; c < _->originImage.width(); ++c, ++i) {
            const QColor& originColor = _->imageColors[i];
            if (originColor.hue() == -1) {
                resultLine[c] = originColor.rgb();
            } else {
                const QColor& newColor = _->shifted(originColor, hueCenter, _->sectorSize);
                resultLine[c] = newColor.rgb();
            }
        }
    }
    return result;
}
